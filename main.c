/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
#include "main.h"
#include "../Drivers/CMSIS/Device/ST/STM32F0xx/Include/stm32f031x6.h"
#include "../Drivers/STM32F0xx_HAL_Driver/Inc/stm32f0xx_hal_gpio.h"
#include "../Drivers/CMSIS/Core/Include/core_cm0.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Global variables ---------------------------------------------------------*/

/* USER CODE BEGIN GV */


//flag for interrupt
volatile uint8_t flag = 0;

//position of hour line 0-11
volatile uint8_t hour_position = 2;

//position of minute line 0-59
volatile uint8_t minute_position = 58;

/*
 * The counter is used in the timer interrupt after every 6s (trigger of the interrupt) the counter is incremented
 * when the counter reaches the number 10 we know its been a minute and we can move the minute hand of the clock
 * when the minute hand of the clock reaches 60 we know we can increment the hour
*/
volatile uint8_t timer_counter_minute = 0;

uint8_t sevenhalf_minute_line = 0; //10 lines of these total

uint8_t one_minutehalf_line = 1; //4 of these in between each 7.5 minute line

uint8_t hour_line = 2; //hour hand (hour hand for intermediate hours in between 7.5 minute lines (for 
//instance 1 and 2, will show relative to their respective positions, 1 will be showed right before the first 7.5 minute line,
//2 right after the first 7.5 minute line, and 3 will be on the 2nd 7.5 minute line, etc..)

uint8_t minitehalf_line = 3; //minute hand
//Small error in the minutes, it is just rounded to give an approximation of the time
//Clock face is not traditional, with some rounding errors, but it is still easy to make out a relative time

uint8_t RESET_LED = 4;
/* USER CODE END GV */

// For store tick counts in us
volatile __IO uint32_t usTicks;

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void EXTI0_1_IRQHandler(void);
void TIM14_IRQHandler(void);

//We want this function to configure the prescalar to have a counting time of 6s
void TIM14_Init(void);

//Handles the drawing of the clock face
void Draw_Clock_Face(void);

//Draws hour line
void Draw_Hour_Line(double);

void Draw_SevenHalfMinute_Line(double);

void Draw_MinuteHalf_Line(double);

//Draws minute line
void Draw_Short_Line(double);

void OUTPUT_LED_STATE(uint8_t);

void DelayInit(void);
void DelayUs(uint32_t us);
void DelayMs(uint32_t ms);
void SysTick_Handler(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
    /* USER CODE BEGIN 1 */

    /* USER CODE END 1 */

    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* USER CODE BEGIN Init */

    /* USER CODE END Init */

    /* Configure the system clock */
    SystemClock_Config();

    /* USER CODE BEGIN SysInit */
    __HAL_RCC_GPIOA_CLK_ENABLE(); //enables GPIO AHB bus so we can set and clear registers in GPIO peripherals
    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_TIM14_CLK_ENABLE();

    // Configure the Systick for 25ms ticks
    SysTick->CTRL = 0; // Disable SysTick
    SysTick->LOAD = 0x00258; // Delay value
    SysTick->VAL = 0x0;
    SysTick->CTRL = 0x7; // Enable SysTick with exception generation // and use core clock as source*/



    GPIO_InitTypeDef initGPIOInterrupt = {GPIO_PIN_0,
                                          GPIO_MODE_INPUT,
                                          GPIO_PULLDOWN,
                                          GPIO_SPEED_FREQ_HIGH};

    HAL_GPIO_Init(GPIOA, &initGPIOInterrupt);

    GPIO_InitTypeDef initGPIOToggleA = {GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 |
                                        GPIO_PIN_7 | GPIO_PIN_10,
                                        GPIO_MODE_OUTPUT_PP,
                                        GPIO_PULLDOWN,
                                        GPIO_SPEED_FREQ_HIGH};

    HAL_GPIO_Init(GPIOA, &initGPIOInterrupt);
    HAL_GPIO_Init(GPIOA, &initGPIOToggleA);

    //Initializes timer to generate interrupt every 6 seconds
    TIM14_Init();

    // Routing EXTI0 line to the PA0 line, the bits EXTI0[3:0] set to 0x0, this selects the PA0 line and maps it to the EXTI0 line
    SYSCFG->EXTICR[0] = SYSCFG_EXTICR1_EXTI0_PA;

    //Unmasks the EXTI0 interrupt so it can create an interrupt, it wont be ignored
    EXTI->IMR |= EXTI_IMR_MR0_Msk;

    //Falling edge creates the interrupt
    EXTI->FTSR |= EXTI_FTSR_TR0;

    // Enables the EXTI0 interrupt request line, so we can create interrupts with this line
    NVIC_EnableIRQ(EXTI0_1_IRQn);
    NVIC_EnableIRQ(TIM14_IRQn);
    NVIC_EnableIRQ(SysTick_IRQn);
    NVIC_SetPriority(TIM14_IRQn, 2);
    NVIC_SetPriority(EXTI0_1_IRQn, 2);
    NVIC_SetPriority(SysTick_IRQn, 1);

    /* USER CODE END SysInit */

    /* Initialize all configured peripherals */
    /* USER CODE BEGIN 2 */

    /* USER CODE END 2 */



    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
    while (1)
    {
        //This is used to draw a constant outline of the clock
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);
        /* USER CODE END WHILE */

        if(flag == 1) {
            Draw_Clock_Face();
            flag = 0;
        }

        /* USER CODE BEGIN 3 */
    }
    /* USER CODE END 3 */
}

void Draw_Clock_Face(void) {
    uint8_t i = 0;
    uint8_t num_of_parts = 40;

    /*
     * Draw this once per rotation, we will iterate through and draw short lines every period/60 seconds
     * Draw the minute/hour lines when the iteration i reaches minute/hour variable number
     */
    for(i=0; i<num_of_parts; i++)
    {
        /*Draw_Short_Line(300);
       for (int i = 0; i <= 500; i++){*/
        if (i == minute_position){
            Draw_MinuteHalf_Line(7);
        }
        else if(i%5 == 0)
        {
            if(i == hour_position*5) {
                Draw_Hour_Line(7);
            }
            else {
                Draw_SevenHalfMinute_Line(7);
            }
        }
        else {
            Draw_Short_Line(7);
        }
        DelayUs(7);
    }
}

void OUTPUT_LED_STATE(uint8_t line_type) {

    //7.5 minute line
    if(line_type == 0) {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);
    }
        //1.5 minute line
    else if (line_type == 1) {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);
    }
        //hour hand
    else if (line_type == 2) {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
    }
        //minute hand
    else if (line_type == 3) {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_SET);
    }
    else {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
    }
}

void Draw_SevenHalfMinute_Line(double small_delay)
{
    OUTPUT_LED_STATE(sevenhalf_minute_line);
    DelayUs(small_delay);
    OUTPUT_LED_STATE(RESET_LED);
}

void Draw_Short_Line(double small_delay)
{
    OUTPUT_LED_STATE(1);
    DelayUs(small_delay);
    OUTPUT_LED_STATE(RESET_LED);
}
void Draw_MinuteHalf_Line(double small_delay)
{
    OUTPUT_LED_STATE(minitehalf_line);
    DelayUs(small_delay);
    OUTPUT_LED_STATE(RESET_LED);
}
void Draw_Hour_Line(double small_delay)
{
    OUTPUT_LED_STATE(hour_line);
    DelayUs(small_delay);
    OUTPUT_LED_STATE(RESET_LED);
}

//PA0 Handler
void EXTI0_1_IRQHandler(void) {

    flag = 1;
    // Says the interrupt isnt pending anymore and it has been serviced
    EXTI->PR &= ~(0U);
}

void TIM14_Init(void) {
    TIM14->PSC = (uint16_t) 16000U;
    TIM14->ARR = (uint16_t) 3000U;
    TIM14->DIER |= TIM_DIER_UIE;
    TIM14->CR1 |= TIM_CR1_CEN;
}

/*
 * Triggers every 6 seconds, it will handle:
 * Determining the position of the minute and hour hands of the clock
 */
void TIM14_IRQHandler(void) {

    timer_counter_minute++;

    if(timer_counter_minute == 10) {
        if(minute_position < 59) {
            minute_position++;
        } else {
            minute_position = 0;
            hour_position++;
            if (hour_position == 11) hour_position = 0;
        }
        timer_counter_minute = 0;
    }
    TIM14->SR &= ~(TIM_SR_UIF);
}

//50 us increments
void DelayUs(uint32_t us) {
    usTicks = us;

    while(usTicks);
}

void DelayMs(uint32_t ms) {

    while(ms--){
        DelayUs(20);
    }

}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
    /* USER CODE BEGIN SysTick_IRQn 0 */

    // SysTick_Handler function will be called every 1 us
    if (usTicks != 0)
    {
        usTicks--;
    }

    /* USER CODE END SysTick_IRQn 0 */
    //HAL_IncTick();
    /* USER CODE BEGIN SysTick_IRQn 1 */

    /* USER CODE END SysTick_IRQn 1 */
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

    /** Initializes the CPU, AHB and APB busses clocks
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB busses clocks
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                                  |RCC_CLOCKTYPE_PCLK1;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
    {
        Error_Handler();
    }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */

    /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
